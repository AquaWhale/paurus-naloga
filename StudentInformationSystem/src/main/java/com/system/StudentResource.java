package com.system;


import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.system.model.Student;
import com.system.repository.ClassRepository;
import com.system.repository.StudentRepository;
import com.system.model.Class;

@RestController
public class StudentResource {

	@Autowired
	StudentRepository studentRepository;
	
	@Autowired
	ClassRepository classRepository;

	
	@GetMapping("students")
	public List<Student> getStudents() {
		
		List<Student> students = (List<Student>) studentRepository.findAll();
		
		return students;
	}
	
	@PostMapping("students")
	public Student addStudent(@RequestBody Student s) {
		return studentRepository.save(s);
	}
	
	@GetMapping("class")
	public List<Class> getClasses() {
		List<Class> classes = classRepository.findAll();
		
		return classes;
	}
	
	@GetMapping("class/{name}")
	public Class getSpecificClass(@PathVariable("name") String name) {
		
		Class c = (Class) classRepository.findByClassName(name).get(0);
		
		return c;
	}
	
	@GetMapping("students/{id}")
	public Student getSpecificStudent(@PathVariable("id") int id) {
		Optional<Student> searchedStudent = studentRepository.findById(id);
		Student student = null;
		if(searchedStudent.isPresent())
			student = searchedStudent.get();
		return student;
	}
	
	@PutMapping("students/{id}/{name}")
	public Optional<Student> addClassToStudent(@PathVariable("id") int id, @PathVariable("name") String name) {
		
		Optional<Student> searchedStudent = studentRepository.findById(id);
		Student student = null;
		
		//find class with searched name if it exists or else return nothing
		List<Class> searchedClasses = classRepository.findByClassName(name);
		if (searchedClasses.isEmpty()) return null;
		Class chosenClass = searchedClasses.get(0);
		
		//check if a student exists if not return nothing
		if(searchedStudent.isPresent()) {
			student = searchedStudent.get();
			
			//we add a class to the student if he didn't apply to it yet
			Set<Class> appliedClasses = student.getAppliedClasses();
			//check if a student did already apply to a class
			boolean applied = false;
			for (Class c : appliedClasses) {
				if (c.getClassName().equals(name)) applied = true;
			}
			//apply to class only if a student didn't apply to it already
			if (!applied) {
				student.getAppliedClasses().add(chosenClass);
				studentRepository.save(student);
			}
			
		}
		return studentRepository.findById(id);
	}
	
	@DeleteMapping("students/{id}/{name}")
	public Optional<Student> removeClassFromStudent(@PathVariable int id, @PathVariable String name) {
		
		Optional<Student> searchedStudent = studentRepository.findById(id);
		Student student = null;
		
		//find class with searched name if it exists or else return nothing
		List<Class> searchedClasses = classRepository.findByClassName(name);
		if (searchedClasses.isEmpty()) return null;
		//it return the first class because we predict that there is only one class with this name
		Class chosenClass = searchedClasses.get(0);
		
		//check if a student exists if not return nothing
		if(searchedStudent.isPresent()) {
			student = searchedStudent.get();
			
			//we remove the class from the student if he applied for it or else we don't do nothing
			Set<Class> appliedClasses = student.getAppliedClasses();
			//check if a student did already apply to a class
			boolean applied = false;
			for (Class c : appliedClasses) {
				if (c.getClassName().equals(name)) {
					applied = true;
					chosenClass = c;
				}
			}
			//remove the class if the student applied to it
			if (applied) {
				student.getAppliedClasses().remove(chosenClass);
				studentRepository.save(student);
			}
			
		}
		return studentRepository.findById(id);
		
	}
}
