package com.data.DataInsert;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataInsertApplication implements CommandLineRunner{
	
	//JDBC driver name and database URL 
   static final String JDBC_DRIVER = "org.h2.Driver";   
   static final String DB_URL = "jdbc:h2:mem:testdb";  
	   
   //Database credentials 
   static final String USER = "sa"; 
   static final String PASS = ""; 
	
	public static void main(String[] args) {
		SpringApplication.run(DataInsertApplication.class, args);
	}

	@Override
    public void run(String...args) throws Exception {
        
		Connection connection = null; 
	    PreparedStatement statement = null;
	    
	    try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = 
                new FileReader("fo_random.txt");

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);
            
            String line = bufferedReader.readLine();
            try { 
	   	         //Register JDBC driver for H2 database 
	   	         Class.forName(JDBC_DRIVER); 
	   	             
	   	         //Open a JDBC connection to a H2 database 
	   	         System.out.println("Connecting to a selected database..."); 
	   	         connection = DriverManager.getConnection(DB_URL,USER,PASS); 
	   	         System.out.println("Connected database successfully..."); 
	   	         
	   	         //Execute a query
	   	         String sql = "insert into person (match_id, market_id, outcome_id, specifiers, date_insert) values (?, ?, ?, ?, ?)";
	   	         
	   	         statement = connection.prepareStatement(sql);
	   	         
	   	         //Batch insertion
	   	         final int batchSize = 1000;
	   	         int count = 0;
	   	         
	   	         //Read a line from file and insert to database
		   	     while((line = bufferedReader.readLine()) != null) {
		                String[] parts = line.split("\\|");
		                statement.setString(1, parts[0]);
		   	         	statement.setInt(2, Integer.parseInt(parts[1]));
		   	         	statement.setString(3, parts[2]);
		   	         	if (parts.length < 4) {
		   	         		statement.setString(4, "");
		   	         	}
		   	         	else statement.setString(4, parts[3]);
	   	         		statement.setTimestamp(5, getCurrentTimeStamp());
	   	         		statement.addBatch();
	   	         		
		   	         	if(++count % batchSize == 0) {
		   	         		statement.executeBatch();
		   	         	}
		         }
	   	         
	   	         statement.executeBatch(); 
	   	         
	   	         System.out.println("Inserted records into the table..."); 
	   	         
	   	         //Clean-up environment 
	   	         statement.close(); 
	   	         connection.close();        
   	  
	   	    } catch(SQLException se) { 
	   	         //Handle errors for JDBC 
	   	         se.printStackTrace(); 
	   	    } catch(Exception e) { 
	   	         //Handle errors for Class.forName 
	   	         e.printStackTrace(); 
	   	    } finally { 
	   	         //finally block used to close resources 
	   	         try{ 
	   	            if(statement!=null) statement.close(); 
	   	         } catch(SQLException se2) { 
	   	         } // nothing we can do 
	   	         try { 
	   	            if(connection!=null) connection.close(); 
	   	         } catch(SQLException se){ 
	   	            se.printStackTrace(); 
	   	         } //end finally try 
	   	    } //end try 
	   	    System.out.println("Goodbye!");		
  
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(ex);                
        }
        catch(IOException ex) {
            System.out.println(ex);
        }
	    
	    
    }
	
	//Method that returns current timestamp
	private static java.sql.Timestamp getCurrentTimeStamp() {

		java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime());

	}
}
