package com.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.system.model.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {
	
}
