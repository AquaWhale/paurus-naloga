CREATE TABLE student (
	student_id INTEGER PRIMARY KEY AUTO_INCREMENT,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL
);

CREATE TABLE class (
	class_id INTEGER AUTO_INCREMENT,
	class_name VARCHAR(50) NOT NULL,
	class_difficulty VARCHAR(50) NOT NULL
);

CREATE TABLE student_class (
	student_id INTEGER NOT NULL,
	class_id INTEGER NOT NULL,
	FOREIGN KEY (student_id) REFERENCES student(student_id),
	FOREIGN KEY (class_id) REFERENCES class(class_id),
	PRIMARY KEY (student_id, class_id)
);