package com.system.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.system.model.Class;

public interface ClassRepository extends JpaRepository<Class, Integer> {
	List<Class> findByClassName(String name);
}

