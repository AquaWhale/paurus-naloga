package com.data.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.data.model.Person;

@Repository
public class PersonRepository {
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	
	public int addPerson() {
		return jdbcTemplate.update("insert into person (match_id, market_id, outcome_id, specifiers) " + "values(?, ?, ?, ?)", new Object[] {
	            "a", 1, "a", "a"
	        }, new BeanPropertyRowMapper < Person > (Person.class));
	}
	
}
