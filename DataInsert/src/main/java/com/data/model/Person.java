package com.data.model;

public class Person {

	private String matchId;
	private int marketId;
	private String outcomeId;
	private String specifiers;
	
	public String getMatchId() {
		return matchId;
	}
	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}
	public int getMarketId() {
		return marketId;
	}
	public void setMarketId(int marketId) {
		this.marketId = marketId;
	}
	public String getOutcomeId() {
		return outcomeId;
	}
	public void setOutcomeId(String outcomeId) {
		this.outcomeId = outcomeId;
	}
	public String getSpecifiers() {
		return specifiers;
	}
	public void setSpecifiers(String specifiers) {
		this.specifiers = specifiers;
	}
	
	
}
