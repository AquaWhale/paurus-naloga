package com.system.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.system.model.Student;

@Entity
public class Class {
	
	@Id
	@Column(name="class_id")
	private int classId;
	
	private String className;
	
	private String classDifficulty;
	
	@ManyToMany(mappedBy = "appliedClasses")
	private Set<Student> members;
	
	public int getClassId() {
		return classId;
	}
	public void setClassId(int classId) {
		this.classId = classId;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getClassDifficulty() {
		return classDifficulty;
	}
	public void setClassDifficulty(String classDifficulty) {
		this.classDifficulty = classDifficulty;
	}
	
	@JsonIgnore
	public Set<Student> getMembers() {
		return members;
	}
	public void setMembers(Set<Student> members) {
		this.members = members;
	}
	
}
