package com.system.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.JoinColumn;

import com.system.model.Class;

@Entity
public class Student {
	
	@Id
	@Column(name="student_id")
	private int studentId;
	private String firstName;
	private String lastName;
	
	@ManyToMany
	@JoinTable(
			  name = "student_class", 
			  joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "student_id"), 
			  inverseJoinColumns = @JoinColumn(name = "class_id", referencedColumnName = "class_id"))
	private Set<Class> appliedClasses;
	
	
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Set<Class> getAppliedClasses() {
		return appliedClasses;
	}
	public void setAppliedClasses(Set<Class> appliedClasses) {
		this.appliedClasses = appliedClasses;
	}
	
	
	
}
