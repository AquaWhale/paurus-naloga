
INSERT INTO student (first_name, last_name) VALUES 
	('Bill', 'Gates'),
	('Reese', 'Kripto'),
	('Omar', 'Krinch');

	
INSERT INTO class (class_name, class_difficulty) VALUES
	('Spring boot course', 'hard'),
	('Java', 'medium'),
	('Web development course', 'hard'),
	('History', 'easy'),
	('Mathematics', 'average'),
	('Biology', 'average'),
	('Athletics', 'average'),
	('Robotics', 'hard'),
	('Python', 'medium');
	
INSERT INTO student_class (student_id, class_id) VALUES
	(1, 1),
	(1, 2),
	(1, 3),
	(2, 5),
	(2, 7),
	(2, 8),
	(3, 4),
	(3, 5);